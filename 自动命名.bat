@echo off & setlocal enabledelayedexpansion
::名称：自动命名工具
::功能：对班级作业进行自动格式化命名、输出未交名单
::用法：本文件与需命名文件放在同一目录下，配合 班级名单.txt 使用；
::          命名工具变量和命名格式按需求修改；
::          设置完成后双击本文件运行
::版本：v1.2 Beta
::编辑：ZETTA  2022.06.08
::
::-------------------命名工具变量------------------------+
::IDNUM表示取学号的位数，0为全部，-3为学号后三位
set IDNUM=-3
set CLASS=19计算机x班
set COURSE=课程名称
set SUFFIX=第x次作业
set list=班级名单.txt
set dlist=未交名单.txt
::---------------------------------------------------------+
set TAG=N
set NAME=xxx
set LIST=%list%
set DLIST=%dlist%

echo.
echo.
echo ####自动命名工具####
echo.
echo.
::脚本所在目录是否存在班级名单
if not exist %LIST% (
echo 找不到 %LIST% !
echo 命名失败!
goto end
) 

echo. > %DLIST%
::获取班级名单里的ID和NAME
for /f "tokens=1-2" %%i in (%LIST%) do (
set NAME=%%i
set ID=%%j
::-----------------------------文件命名格式---------------------------------+
set PATTERN=!ID:~%IDNUM%! !CLASS! !NAME! !COURSE!!SUFFIX!
::---------------------------------------------------------------------------+
if exist *!NAME!* (
for %%a in (*!NAME!*) do (
echo %%~na%%~xa
echo 重命名为
echo !PATTERN!%%~xa
ren "%%a" "!PATTERN!%%~xa"
echo.
)
) else (
echo !NAME!没有交作业
echo.
echo !NAME! !ID! >> %DLIST%
set TAG=Y
)
)

echo.
echo ######命名完成######
echo.


if !TAG!==N (
echo ######无未交名单######
echo.
del 未交名单.txt
) else (
echo ######未交名单######
echo.
for /f %%i in (%DLIST%) do echo %%i
echo.
echo ##################
echo.
)

echo.
echo OK！
echo.

:end
pause > nul