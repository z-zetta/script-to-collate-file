# 文件批整理

#### 介绍
主要用于学习通作业包进行整理，解压、删除冗余文件、重命名、输出未交名单
免去了一个个作业包解压的麻烦


#### 使用说明

1.  文件内含有中文字符，需保存为ANSI编码格式
2.  “学习通作业工具(绑定7z).bat”需要绑定7z解压工具使用，暂未开发使用其他压缩工具的版本
    使用时需要修改为你的7z.exe路径（具体打开bat文件查看）
3.  “自动命名.bat”提供了学号、班级、姓名、课程、后缀等变量的设置，根据需求进行修改，最终文件命名格式为PATTERN，根据需求删除移动变量即可
4.  脚本与待修改文件处于同一目录使用，需配合“班级文件.txt”使用

#### 待改进
1.  优化代码，将“学习通作业工具(绑定7z).bat”中的自动命名代码删除，直接调用“自动命名.bat”
2.  拓展WinRAR版本，更具适用性
3.  为非计算机人士优化使用体验
