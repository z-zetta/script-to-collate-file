@echo off & setlocal enabledelayedexpansion
::名称：学习通作业工具
::功能：帮助学委解压学习通下载的作业包
::用法：本文件与作业压缩包放在同一目录下
::          解压工具变量首次设置好即可；
::          设置完成后双击本文件运行
::版本：v1.2 Beta
::编辑：ZZT   2022.06.09
::
::
::---------------------解压工具变量----------------------+
::解压后删除原压缩包 Y/N
set AUTODEL=N
::7z压缩器exe路径
set ZIP=D:\7-Zip\7z.exe
::需要解压的文件所在目录，默认当前目录
set CUR_PATH=%cd%
::删除含有以下关键词文件，使用正则表达式
set DELETE=其他-其他*,*数学*,*地理*
::---------------------------------------------------------+
echo.
echo ######学习通作业解压工具######
echo.
echo.
echo 检测到zip文件如下:
echo.
for %%i in (*.zip) do (echo  - %%~ni)
echo.
set /p FILENAME=输入zip文件名:

::文件完整路径
set FILE=%CUR_PATH%\%FILENAME%

::创建文件夹
if exist %FILE% (
rmdir /q /s %FILE%
rd /q /s %FILE%
)
md %FILE%

::解压第一层压缩
%ZIP% x %FILE%.zip -o%FILE%

::解压第二层压缩并删除压缩包和无关文件
cd %FILE%
for %%i in (*.zip) do %ZIP% x %%i & del %%i
for %%i in (%DELETE%) do del %%i

cd ..
if %AUTODEL%==Y del %FILE%.zip

echo.
echo 解压完成！
echo.

pause > nul